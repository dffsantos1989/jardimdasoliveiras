<?php

namespace App\Form;

use App\Entity\BannerTranslation;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BannerTranslationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('is_active', CheckboxType::class, [
                'label' => 'active',
                'required' => false,
                'attr' => ['class' => 'w3-check'],
            ])
            ->add('name', TextType::class, [
                'label' => 'text',
                'required' => false,
                'attr' => ['class' => 'w3-input w3-border w3-white w3-hide'],
            ])
            ->add('description', TextType::class, [
                'label' => 'description',
                'required' => false,
                'attr' => ['class' => 'w3-input w3-border w3-white'],
            ])
        ;
    }

    /*
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => BannerTranslation::class,
        ));
    }*/
}
