<?php

namespace App\Form;

use App\Entity\Countries;
use App\Entity\PropertyBulletinGuest;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class PropertyBulletinGuestType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('name', TextType::class, [
                'label' => 'property_bulletin.name',
                'required' => true,
                'attr' => ['class' => 'w3-input w3-border u-name', 'placeholder' => 'property_bulletin.name', 'pattern' => '/^[a-zçãáàéêíõôóúA-ZÇÃÁÀÉÊÍÕÔÓÚ][a-zçãáàéêíõôóúA-ZÇÃÁÀÉÊÍÕÔÓÚ’\- ]{1,39}$/'],
        ])
        ->add('lastName', TextType::class, [
            'label' => 'property_bulletin.last_name',
            'required' => true,
            'attr' => ['class' => 'w3-input w3-border', 'placeholder' => 'property_bulletin.last_name', 'pattern' => '/^[a-zçãáàéêíõôóúA-ZÇÃÁÀÉÊÍÕÔÓÚ][a-zçãáàéêíõôóúA-ZÇÃÁÀÉÊÍÕÔÓÚ’\- ]{1,39}$/'],
        ])
        ->add('birthDate', DateType::class, [
            'widget' => 'single_text',
            'html5' => false,
            'format' => 'yyyy-MM-dd',
            'label' => 'property_bulletin.birth_day',
            'required' => true,
            'attr' => ['class' => 'w3-input w3-border', 'placeholder' => 'property_bulletin.birth_day', 'readonly' => 'readonly'],
        ])

        ->add('email', EmailType::class,
            [
                'required' => false,
                'label' => 'Email',
                'attr' => ['class' => 'w3-input w3-border', 'placeholder' => 'Email'],
            ])

        ->add('telephone', TelType::class,
        [
            'required' => false,
            'label' => 'property_bulletin.telephone',
            'attr' => ['class' => 'w3-input w3-border', 'placeholder' => 'property_bulletin.telephone'],
        ])

        ->add('identificationType', ChoiceType::class, [
            'label' => 'property_bulletin.identification_type',
            'placeholder' => 'property_bulletin.identification_type',
            'required' => true,
            'choices' => [
                'property_bulletin.passport' => 'P',
                'property_bulletin.id_card' => 'B',
                'property_bulletin.other' => 'O',
            ],
            'attr' => ['class' => 'w3-select w3-border'],
        ])
        ->add('identificationNumber', TextType::class, [
            'label' => 'property_bulletin.identification_number',
            'required' => true,
            'attr' => ['class' => 'w3-input w3-border', 'placeholder' => 'property_bulletin.identification_number', 'pattern' => '/^[a-zA-Z0-9]{1,16}$/'],
        ])
        ->add('countryOrigin', EntityType::class, [
            'class' => Countries::class,
            'choice_label' => 'name',
            'placeholder' => 'property_bulletin.country_origin',
            'required' => false,
            'attr' => ['class' => 'w3-select w3-border '],
            'constraints' => [
                new NotBlank(),
            ],
        ])
        ->add('countryResidence', EntityType::class, [
            'class' => Countries::class,
            'choice_label' => 'name',
            'placeholder' => 'property_bulletin.country_residence',
            'required' => false,
            'attr' => ['class' => 'w3-select w3-border '],
            'constraints' => [
                new NotBlank(),
            ],
        ])
        ->add('countryOriginIdentification', EntityType::class, [
            'class' => Countries::class,
            'choice_label' => 'name',
            'required' => false,
            'placeholder' => 'property_bulletin.country_origin_identification',
            'attr' => ['class' => 'w3-select w3-border '],
            'constraints' => [
                new NotBlank(),
            ],
        ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => PropertyBulletinGuest::class,
            'property' => null,
        ]);
    }

    public function getBlockPrefix()
    {
        return 'PropertyBulletinGuestType';
    }
}
