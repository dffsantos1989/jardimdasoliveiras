<?php

namespace App\Form;

use App\Entity\Locales;
use App\Entity\Rgpd;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RgpdType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('rgpdhtml', HiddenType::class,
            [
                'required' => false,
            ])
            ->add('locales', EntityType::class, [
                    'class' => Locales::class,
                    'choice_label' => 'name',
                    'placeholder' => 'Local',
                    'label' => 'Local',
                    'attr' => ['class' => 'w3-input w3-select w3-border w3-white'],
            ])
            ->add('name', TextType::class,
                [
                'label' => 'name',
                'required' => false,
                'attr' => ['class' => 'w3-input w3-border w3-white', 'placeholder' => 'name'],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Rgpd::class,
        ]);
    }
}
