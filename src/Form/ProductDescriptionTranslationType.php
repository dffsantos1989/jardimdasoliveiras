<?php

namespace App\Form;

use App\Entity\Locales;
use App\Entity\ProductDescriptionTranslation;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProductDescriptionTranslationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('locales', EntityType::class, [
            'class' => Locales::class,
            'choice_label' => 'filename',
            'placeholder' => false,
            'label' => false,
            'attr' => ['class' => 'w3-input w3-select w3-border w3-white locales w3-hide'],
        ])

            // ->add('locales', EntityType::class, array(
            //     'label'=> 'locales',
            //     'required' => false,
            //     'attr' => ['class' => 'w3-input w3-border w3-white','placeholder'=>'locales']
            // ))

            ->add('name', TextType::class, [
                'label' => 'title',
                'required' => false,
                'attr' => ['class' => 'w3-input w3-border w3-white', 'placeholder' => 'title'],
            ])

            // ->add('html', TextType::class,array(
            //     'required' => false
            // ))
            ->add('html', TextareaType::class, [
                'label' => 'text',
                'required' => false,
                'attr' => ['class' => 'ckeditor w3-input w3-border w3-white', 'placeholder' => 'text...'],
            ])
            ->add('check_in_text', HiddenType::class, [
                'required' => false,
            ])

            ->add('seo_description', TextareaType::class, [
                'label' => 'SEO',
                'required' => false,
                'attr' => ['class' => 'w3-input w3-border w3-white', 'placeholder' => 'text...'],
            ])
            // ->add('check_in_text', TextareaType::class,array(
            //     'label'=> 'check_in_text',
            //     'required' => false,
            //     'attr' => ['class' => 'w3-input w3-border w3-white','placeholder'=>'text...']
            // ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ProductDescriptionTranslation::class,
        ]);
    }

    public function getBlockPrefix()
    {
        return 'ProductDescriptionTranslationType';
    }
}
