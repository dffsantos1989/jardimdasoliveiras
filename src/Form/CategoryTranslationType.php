<?php

namespace App\Form;

use App\Entity\Category;
use App\Entity\CategoryTranslation;
use App\Entity\Locales;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CategoryTranslationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('locales', EntityType::class, [
                'class' => Locales::class,
                'choice_label' => 'name',
                'placeholder' => 'Local',
                'label' => 'Local',
                'attr' => ['class' => 'w3-input w3-select w3-border w3-white'],
            ])
            ->add('category', EntityType::class, [
                'class' => Category::class,
                'choice_label' => 'id',
                'placeholder' => 'category',
                'label' => 'category',
                'attr' => ['class' => 'w3-input w3-select w3-border w3-white'],
            ])
            ->add('name', TextType::class, [
                'label' => 'name',
                'required' => false,
                'attr' => ['class' => 'w3-input w3-border w3-white', 'placeholder' => 'name',
                'pattern' => '/^[a-zçãáàéêíõôóúA-ZÇÃÁÀÉÊÍÕÔÓÚ][a-zçãáàéêíõôóúA-ZÇÃÁÀÉÊÍÕÔÓÚ’\- ]{1,39}$/'],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => CategoryTranslation::class,
        ]);
    }
}
