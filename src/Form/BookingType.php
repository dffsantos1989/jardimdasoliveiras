<?php

namespace App\Form;

use App\Entity\Booking;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BookingType extends AbstractType
{
    private function color()
    {
        return [
        'w3-text-black',
        'w3-t-tour',
        'w3-text-blue',
        'w3-text-indigo',
        'w3-text-teal',
        'w3-text-blue-gray',
        'w3-text-deep-purple',
        'w3-text-cyan',
        'w3-text-aqua',
        'w3-text-brown',
        'w3-text-deep-orange',
        ];
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('adult', IntegerType::class,
            [
                'label' => false,
                'required' => false,
                'attr' => ['class' => 'w3-input w3-padding-16', 'placeholder' => 'adult', 'min' => '0'],
            ])
        ->add('children', IntegerType::class,
            [
                'label' => false,
                'required' => false,
                'attr' => ['class' => 'w3-input w3-padding-16', 'placeholder' => 'children', 'min' => '0'],
            ])
        ->add('baby', IntegerType::class,
            [
                'label' => false,
                'required' => false,
                'attr' => ['class' => 'w3-input w3-padding-16', 'placeholder' => 'baby', 'min' => '0'],
            ])
        ->add('notes', TextareaType::class,
            [
                'label' => false,
                'required' => false,
                'attr' => ['class' => 'w3-input w3-padding-16', 'placeholder' => 'notes'],
            ])
        ->add('submit', SubmitType::class,
            [
                'label' => 'submit',
                'attr' => ['class' => 'w3-btn w3-block w3-border w3-green w3-margin-top'],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Booking::class,
        ]);
    }
}
