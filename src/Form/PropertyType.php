<?php

namespace App\Form;

use App\Entity\Property;
use App\Entity\User;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PropertyType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('user', EntityType::class, [
            'class' => User::class,
            'choice_label' => 'username',
            'data' => $options['user'],
            'attr' => ['class' => 'w3-hide'],
            'label' => false,
        ])
        ->add('name', TextType::class, [
            'label' => 'property.name',
            'required' => true,
            'attr' => [
                'class' => 'w3-input w3-border w3-white',
                'placeholder' => 'property.name',
                'pattern' => '/^[a-zA-Z0-9 ]{1,40}$/',
                'maxlength' => '40',
            ],
        ])
        ->add('hotelUnitCode', TextType::class, [
            'label' => 'property.hotel_unit_code',
            //'label'=> false,
            'required' => false,
            'attr' => [
                'class' => 'w3-input w3-border w3-white ',
                'placeholder' => 'property.hotel_unit_code',
                'pattern' => '/^[0-9]{9}$/',
                'maxlength' => '9',
                'data-parsley-remote' => '',
                'data-parsley-remote-validator' => 'nif',
                ],
        ])
        ->add('establishment', TextType::class, [
            'label' => 'property.establishment',
            //'label'=> false,
            'required' => false,
            'attr' => [
                'class' => 'w3-input w3-border w3-white  ',
                'placeholder' => 'property.establishment',
                'pattern' => '/^[0-9]{1,4}$/',
                'maxlength' => '4',
                ],
        ])
        ->add('sefApiKey', TextType::class, [
            'label' => 'property.sef_api_key',
            //'label'=> false,
            'required' => false,
            'attr' => ['class' => 'w3-input w3-border w3-white', 'placeholder' => 'property.sef_api_key'],
        ])
        ->add('deposit', TextType::class, [
            'label' => 'property.deposit',
            //'currency'=>'',
            'required' => false,
            'attr' => [
                    'class' => 'w3-input w3-border w3-white',
                    'scale' => 2,
                    'placeholder' => '0.00',
                    'pattern' => '/^[0-9]*.[0-9]{2}$/',
                ],

            //'label_attr' => ['class' => 'w3-hide']
        ])
        ->add('guestBulletin', CheckboxType::class, [
            'label' => 'property.guest_bulletin',
            'required' => false,
            'attr' => ['class' => 'w3-check',
            'data-parsley-mincheck' => '1',
            'data-parsley-multiple' => 'housing_bulletin',
            'data-parsley-required-message' => $options['housing_bulletin'],
            ],
        ])
        ->add('securityDeposit', CheckboxType::class, [
            'label' => 'property.security_deposit',
            'required' => true,
            'attr' => ['class' => 'w3-check',
            'data-parsley-mincheck' => '1',
            'data-parsley-multiple' => 'housing_bulletin',
            'data-parsley-errors-container' => '#message-holder',
            'data-parsley-required-message' => $options['housing_bulletin'],
            ],
        ])
        ->add('details', CollectionType::class, [
            'entry_type' => PropertyDetailsType::class,
            'entry_options' => ['label' => false],
            'allow_add' => true,
            'allow_delete' => true,
            'label' => false,
            'required' => true,
            'by_reference' => false,
            'prototype' => true,
            'attr' => [
                'class' => 'details',
            ],
        ])
        ->add('translation', CollectionType::class, [
            'entry_type' => PropertyTranslationType::class,
            'entry_options' => ['label' => false],
            'allow_add' => true,
            'allow_delete' => true,
            'label' => false,
            'required' => true,
            'by_reference' => false,
            'prototype' => true,
            'attr' => [
                'class' => 'translation',
            ],
        ])
        ->add('submit', SubmitType::class,
        [
            'label' => 'submit',
            'attr' => ['class' => 'w3-btn w3-block w3-border w3-green w3-margin-top w3-hide'],
        ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Property::class,
            'user' => null,
            'housing_bulletin' => null,
        ]);
    }

    public function getBlockPrefix()
    {
        return 'PropertyType';
    }
}
