<?php

namespace App\Form;

use App\Entity\Locales;
use App\Entity\ProductDetailsTranslation;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProductDetailsTranslationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder

            ->add('name', TextType::class, [
                'label' => 'name',
                'required' => false,
                'attr' => ['class' => 'w3-input w3-border w3-white', 'placeholder' => 'name'],
            ])
            // ->add('text', TextareaType::class,array(
            //     'label'=> 'text',
            //     'required' => false,
            //     'attr' => ['class' => 'w3-input w3-border w3-white','placeholder'=>'text']
            // ))
            // ->add('text', FroalaEditorType::class,array(
            //     "language" => "pt",
            //     "toolbarInline" => true,
            //     "tableColors" => [ "#FFFFFF", "#FF0000" ],
            //     "saveParams" => [ "id" => "myEditorField" ],

            //     'label'=> 'text',
            //     'required' => false,
            //     'attr' => ['class' => 'w3-input w3-border w3-whitev','placeholder'=>'text']
            // ))
            ->add('text', TextareaType::class, [
                'label' => 'text',
                'required' => false,
                'attr' => ['class' => 'ckeditor w3-input w3-border w3-white', 'placeholder' => 'text'],
            ])

            ->add('locales', EntityType::class, [
                'class' => Locales::class,
                'choice_label' => 'filename',
                'placeholder' => false,
                'label' => false,
                'attr' => ['class' => 'w3-input w3-select w3-border w3-white locales-details w3-hide'],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ProductDetailsTranslation::class,
        ]);
    }

    /*
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => BannerTranslation::class,
        ));
    }*/

    public function getBlockPrefix()
    {
        return 'ProductDetailsTranslationType';
    }
}
