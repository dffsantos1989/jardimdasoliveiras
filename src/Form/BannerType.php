<?php

namespace App\Form;

use App\Entity\Banner;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BannerType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('is_active', CheckboxType::class, [
                'label' => 'active',
                'required' => false,
                'attr' => ['class' => 'w3-check'],
            ])
            // ->add('text_active', CheckboxType::class, array(
            //     'label'    => 'title',
            //     'required' => false,
            //     'attr' => ['class' => 'w3-check']
            // ))
            ->add('description_active', CheckboxType::class, array(
                'label'    => 'description',
                'required' => false,
                'attr' => ['class' => 'w3-check']
            ))
            ->add('image', FileType::class, [
                'label' => 'image',
                'required' => false,
                'attr' => ['class' => 'w3-hide set-image', 'onchange' => 'loadFile(event)'],
            ])
            ->add('submit', SubmitType::class,
            [
                'label' => 'save',
                'attr' => ['class' => 'w3-btn w3-block w3-border w3-green w3-margin-top'],
            ])
        ;
    }

    /*
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Banner::class,
        ));
    }*/
}
