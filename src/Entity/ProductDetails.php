<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="product_details")
 * @ORM\Entity(repositoryClass="App\Repository\ProductDetailsRepository")
 */
class ProductDetails
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;
    /**
     * @ORM\ManyToOne(targetEntity="Product", inversedBy="detail")
     */
    private $product;

    /** @ORM\OneToMany(targetEntity="ProductDetailsTranslation", mappedBy="product_detail", cascade={"persist", "remove"}) */
    private $translation;

    public function __construct()
    {
        $this->translation = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getProduct()
    {
        return $this->product;
    }

    public function setProduct(Product $product)
    {
        $this->product = $product;
    }

    public function addTranslation(ProductDetailsTranslation $translation)
    {
        $translation->setProductDetail($this);
        $this->translation->add($translation);
    }

    public function removeTranslation(ProductDetailsTranslation $translation)
    {
        $this->translation->removeElement($translation);
    }

    public function getTranslation()
    {
        return $this->translation;
    }

    public function setTranslation(ProductDetailsTranslation $translation)
    {
        $this->translation = $translation;
    }

    public function getCurrentTranslation(Locales $locales)
    {
        $txt = [];
        if ($this->getTranslation()) {
            foreach ($this->getTranslation() as $translation) {
                if ($locales->getName() == $translation->getLocales()->getName()) {
                    $txt[] = $translation->getName();
                    $txt[] = $translation->getText();
                }
            }
        }

        return $txt;
    }
}
