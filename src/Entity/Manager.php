<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ManagerRepository")
 */
class Manager extends User
{
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $address;

    /**
     * @ORM\Column(type="string", length=40, nullable=true)
     */
    private $nif;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $location;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $postal_code;

    /**
     * @ORM\Column(type="text", name="stripe_public_key", nullable=true)
     */
    private $stripe_public_key;

    /**
     * @ORM\Column(type="text", name="stripe_secret_key", nullable=true)
     */
    private $stripe_secret_key;

    public function getRoles()
    {
        return ['ROLE_MANAGER'];
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getNif(): ?string
    {
        return $this->nif;
    }

    public function setNif(string $nif): self
    {
        $this->nif = $nif;

        return $this;
    }

    public function getStripePublicKey()
    {
        return $this->stripe_public_key;
    }

    public function setStripePublicKey(string $stripe_public_key)
    {
        $this->stripe_public_key = $stripe_public_key;

        return $this;
    }

    public function getStripeSecretKey()
    {
        return $this->stripe_secret_key;
    }

    public function setStripeSecretKey(string $stripe_secret_key)
    {
        $this->stripe_secret_key = $stripe_secret_key;

        return $this;
    }

    public function getLocation(): ?string
    {
        return $this->location;
    }

    public function setLocation(string $location): self
    {
        $this->location = $location;

        return $this;
    }

    public function getPostalCode(): ?string
    {
        return $this->postal_code;
    }

    public function setPostalCode(string $postal_code): self
    {
        $this->postal_code = $postal_code;

        return $this;
    }

    public function getClassName()
    {
        return (new \ReflectionClass($this))->getShortName();
    }
}
