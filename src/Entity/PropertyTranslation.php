<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="property_translation")
 * @ORM\Entity(repositoryClass="App\Repository\PropertyTranslationRepository")
 */
class PropertyTranslation
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;
    /**
     * @ORM\ManyToOne(targetEntity="Locales")
     */
    private $locales;
    /**
     * @ORM\Column(type="text", name="reasons" , nullable = true)
     */
    private $reasons;
    /**
     * @ORM\Column(type="text", name="instructions", nullable = true)
     */
    private $instructions;

    /** @ORM\ManyToOne(targetEntity="Property", inversedBy="translation") */
    private $property;

    public function getId()
    {
        return $this->id;
    }

    public function getProperty()
    {
        return $this->property;
    }

    public function setProperty(Property $property)
    {
        $this->property = $property;
    }

    public function getLocales()
    {
        return $this->locales;
    }

    public function setLocales(Locales $locales)
    {
        $this->locales = $locales;
    }

    public function getInstructions()
    {
        return $this->instructions;
    }

    public function setInstructions($instructions)
    {
        $this->instructions = str_replace("'", '’', $instructions);
    }

    public function getReasons()
    {
        return $this->reasons;
    }

    public function setReasons($reasons)
    {
        $this->reasons = str_replace("'", '’', $reasons);
    }

    // public function getTranslation(Locales $locales)
    // {

    //     $txt = [];
    // 	//if( $locales === $this->getLocales())
    // 		$txt = ["reasons"=>$this->getReasons(), "email_txt"=>$this->getEmailTxt()];

    //     return $txt;
    // }
}
