<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PropertyRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Property
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", name="token",  unique=true)
     */
    private $token;

    /** @ORM\ManyToOne(targetEntity="User") */
    private $user;

    /**
     * @ORM\Column(type="boolean", name="guest_bulletin")
     */
    private $guest_bulletin;

    /**
     * @ORM\Column(type="boolean", name="security_deposit")
     */
    private $security_deposit;

    /**
     * @ORM\Column(type="string", name="sef_api_key", nullable = true)
     */
    private $sef_api_key;

    /** @ORM\OneToMany(targetEntity="PropertyTranslation", mappedBy="property", cascade={"persist", "remove"}) */
    private $translation;

    /** @ORM\OneToMany(targetEntity="PropertyDetails", mappedBy="property", cascade={"persist", "remove"}) */
    private $details;

    /**
     * @ORM\Column(type="money", name="deposit", options={"unsigned"=true}, nullable=true)
     */
    private $deposit;

    /**
     * @ORM\Column(type="string", name="hotel_unit_code", nullable=true)
     */
    private $hotel_unit_code;

    /**
     * @ORM\Column(type="string", name="establishment", nullable=true)
     */
    private $establishment;

    /**
     * @ORM\Column(type="string", name="name")
     */
    private $name;

    /**
     * @ORM\Column(type="boolean", name="deleted")
     */
    private $deleted = 0;

    public function __construct()
    {
        $this->translation = new ArrayCollection();
        //$this->details = new ArrayCollection();
    }

    public function getTranslation()
    {
        return $this->translation;
    }

    public function setTranslation(ArrayCollection $translation)
    {
        $this->translation = $translation;
    }

    public function addTranslation(PropertyTranslation $translation)
    {
        $translation->setProperty($this);
        $this->translation->add($translation);
    }

    public function removeTranslation(PropertyTranslation $translation)
    {
        $this->translation->removeElement($translation);
    }

    public function getDetails()
    {
        return $this->details;
    }

    public function setDetails($details)
    {
        $this->details = $details;
    }

    public function addDetails(PropertyDetails $details)
    {
        $details->setProperty($this);
        //$this->details->add($details);
    }

    public function removeDetails($details)
    {
        $this->details->removeElement($details);
    }

    public function getId()
    {
        return $this->id;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function setUser(User $user)
    {
        $this->user = $user;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = str_replace("'", '’', $name);
    }

    public function getSefApiKey()
    {
        return $this->sef_api_key;
    }

    public function setSefApiKey($sef_api_key)
    {
        $this->sef_api_key = $sef_api_key;
    }

    public function getSecurityDeposit()
    {
        return $this->security_deposit;
    }

    public function setSecurityDeposit($security_deposit)
    {
        $this->security_deposit = $security_deposit;
    }

    public function getGuestBulletin()
    {
        return $this->guest_bulletin;
    }

    public function setGuestBulletin($guest_bulletin)
    {
        $this->guest_bulletin = $guest_bulletin;
    }

    public function getDeposit()
    {
        return $this->deposit;
    }

    public function setDeposit($deposit)
    {
        $this->deposit = $deposit;
    }

    // public function setDeposit($deposit) {
    // 	$this->deposit = str_replace("'","’",$deposit);
    // }

    public function getBulletinType()
    {
        return $this->bulletin_type;
    }

    public function setBulletinType($bulletin_type)
    {
        $this->bulletin_type = $bulletin_type;
    }

    public function getHotelUnitCode()
    {
        return $this->hotel_unit_code;
    }

    public function setHotelUnitCode($hotel_unit_code)
    {
        $this->hotel_unit_code = $hotel_unit_code;
    }

    public function getEstablishment()
    {
        return $this->establishment;
    }

    public function setEstablishment($establishment)
    {
        $this->establishment = $establishment;
    }

    public function getToken()
    {
        return $this->token;
    }

    public function setToken($token)
    {
        $this->token = $token;
    }

    public function getDeleted()
    {
        return $this->deleted;
    }

    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;
    }

    public function getCurrentTranslation(Locales $locales)
    {
        $txt = '';

        if ($this->getTranslation()) {
            foreach ($this->getTranslation() as $translation) {
                if ($locales->getName() == $translation->getLocales()->getName()) {
                    $txt = ['reasons' => $translation->getReasons(), 'email_txt' => $translation->getInstructions()];
                }
            }
        }

        return $txt;
    }
}
