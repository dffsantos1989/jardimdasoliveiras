<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PropertyBulletinRepository")
 */
class PropertyBulletin
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /** @ORM\Column(type="datetime", name="check_in")
     *
     */
    private $check_in;

    /** @ORM\Column(type="datetime", name="check_out")
     *
     */
    private $check_out;

    /** @ORM\Column(type="integer", name="guest_number")
     *
     */
    private $guest_number;

    /**
     * @ORM\Column(type="string", length=100, name="book_id", unique = true)
     * @Assert\NotBlank(message="book_id")
     */
    private $book_id;

    /**
     *@ORM\ManyToOne(targetEntity="Property") */
    private $property;

    /**
     *@ORM\OneToMany(targetEntity="PropertyBulletinGuest", mappedBy="property_bulletin", cascade={"persist", "remove"}) */
    private $guest;

    /** @ORM\Column(type="boolean", name="marketing", nullable=true, options={"default":0}) */
    private $marketing;

    public function getMarketing()
    {
        return $this->marketing;
    }

    public function setMarketing($marketing)
    {
        $this->marketing = $marketing;
    }

    public function __construct()
    {
        $this->guest = new ArrayCollection();
    }

    public function addGuest(PropertyBulletinGuest $guest)
    {
        $guest->setPropertyBulletin($this);
        $this->guest->add($guest);
    }

    public function removeGuest(PropertyBulletinGuest $guest)
    {
        $this->guest->removeElement($guest);
    }

    public function getGuest()
    {
        return $this->guest;
    }

    public function setGuest(ArrayCollection $guest)
    {
        $this->guest = $guest;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getBookId()
    {
        return $this->book_id;
    }

    public function setBookId($book_id)
    {
        $this->book_id = $book_id;
    }

    public function getProperty()
    {
        return $this->property;
    }

    public function setProperty(Property $property)
    {
        $this->property = $property;
    }

    public function getCheckIn()
    {
        return $this->check_in;
    }

    public function setCheckIn($check_in)
    {
        $this->check_in = $check_in;
    }

    public function getCheckOut()
    {
        return $this->check_out;
    }

    public function setCheckOut($check_out)
    {
        $this->check_out = $check_out;
    }

    public function getGuestNumber()
    {
        return $this->guest_number;
    }

    public function setGuestNumber($guest_number)
    {
        $this->guest_number = $guest_number;
    }
}
