<?php

namespace App\Repository;

use App\Entity\Category;
use App\Entity\Product;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Product|null find($id, $lockMode = null, $lockVersion = null)
 * @method Product|null findOneBy(array $criteria, array $orderBy = null)
 * @method Product[]    findAll()
 * @method Product[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Product::class);
    }

    public function findBySomething($highlight = null, Category $category = null)
    {
        $dql = 'SELECT p, c
        FROM App\Entity\Product p
        JOIN p.category c
        WHERE p.isActive = :active
        AND (c.is_active = :active)';
        if ($highlight) {
            $dql .= ' AND p.highlight = :highlight';
        }

        $dql .= $category?" AND c = :category ORDER BY p.orderBy ASC" :" AND c.id != 2 ORDER BY p.orderBy ASC";
        // $dql .= " AND t.status !='canceled' AND b.createdAt >= :dateIni AND b.createdAt <= :dateEnd ORDER BY b.user ASC";
        $query = $this->getEntityManager()->createQuery($dql)
            ->setParameter('active', true);

        //$query = $this->getEntityManager()->createQuery($dql)
        if ($category) {
            $query->setParameter('category', $category);
        }
        if ($highlight) {
            $query->setParameter('highlight', $highlight);
        }

        return $query->getResult();
    }

    /*
    public function findBySomething($value)
    {
        return $this->createQueryBuilder('b')
            ->where('b.something = :value')->setParameter('value', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
}
