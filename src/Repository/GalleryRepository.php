<?php

namespace App\Repository;

use App\Entity\Gallery;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Gallery|null find($id, $lockMode = null, $lockVersion = null)
 * @method Gallery|null findOneBy(array $criteria, array $orderBy = null)
 * @method Gallery[]    findAll()
 * @method Gallery[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GalleryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Gallery::class);
    }

    public function findByCategory($localID)
    {
        $conn = $this->getEntityManager()->getConnection();
        $sql = 'SELECT g.image,ct.name FROM category c
            JOIN gallery g
            ON (c.id = g.category_id)
            JOIN category_translation ct
            ON (c.id = ct.category_id)
            WHERE c.is_Active = :status
            AND ct.locales_id = :local
            ORDER BY ct.name ASC
            ';
        $stmt = $conn->prepare($sql);
        $stmt->execute(['status' => '1', 'local' => $localID]);
        $e = $stmt->fetchAll();

        $cat = $e[0]['name'];
        $images = [];
        $c = [];

        foreach ($e as $category) {
            $c[] = $category['name'];
        }

        $u = array_unique($c);

        foreach ($u as $name) {
            $images = [];
            foreach ($e as $category) {
                if ($name == $category['name']) {
                    $images[] = $category['image'];
                }
            }
            $gc[] = ['images' => $images, 'name' => $name];
        }

        return $gc;
    }
}
