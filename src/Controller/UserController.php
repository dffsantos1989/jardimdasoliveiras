<?php

namespace App\Controller;

use App\Entity\Admin;
use App\Entity\Company;
use App\Entity\Manager;
use App\Entity\SuperUser;
use App\Entity\User;
use App\Form\AdminType;
use App\Form\ChangePasswordType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class UserController extends AbstractController
{
    public function listUser(Request $request, ValidatorInterface $validator)
    {
        $loggedUser = $this->getUser();
        $users = [];
        $role = $request->request->get('role');

        $em = $this->getDoctrine()->getManager();
        $admins = $em->getRepository(Admin::class)->findAll();
        $managers = $em->getRepository(Manager::class)->findAll();

        if ($loggedUser instanceof SuperUser) {
            $superusers = $em->getRepository(SuperUser::class)->findAll();
            $users = array_merge($this->dataUser($admins), $this->dataUser($superusers), $this->dataUser($managers));
        } elseif ($loggedUser instanceof Admin) {
            $users = array_merge($this->dataUser($admins), $this->dataUser($managers));
        } else {
            $users = $this->dataUser($loggedUser);
            // dd($loggedUser,$users);
            // exit;
        }

        // var_dump($this->getEntityName($loggedUser) != "App\Entity\Manager");
        // exit;

        return $this->render('admin/list-users.html', [
            'users' => $users,
            'loggedUserInstance' => $this->getEntityName($loggedUser),
            ]
        );
    }

    public function statusUser(Request $request)
    {
        $response = [];

        $id = $request->request->get('id');
        $status = $request->request->get('status');
        $role = $request->request->get('role');

        $em = $this->getDoctrine()->getManager();
        'ROLE_ADMIN' == $role ? $user = $em->getRepository(Admin::class)->find($id) : $user = $em->getRepository(Manager::class)->find($id);

        if (!$user) {
            $reponse = ['message' => 'fail', 'data' => 'Utilizador Não encontrado ', 'request' => ''];
        } else {
            $user->setStatus($status);
            $em->flush();
            $response = ['message' => 'success', 'data' => $user->getStatus(), 'request ' => $status];
        }

        return new JsonResponse($response);
    }

    public function deleteUser(Request $request)
    {
        $response = [];

        $id = $request->request->get('id');
        $role = $request->request->get('role');
        $em = $this->getDoctrine()->getManager();
        'ROLE_ADMIN' == $role ? $user = $em->getRepository(Admin::class)->find($id) : $user = $em->getRepository(Manager::class)->find($id);

        if (!$user) {
            $response = ['message' => 'fail', 'data' => 'Utlizador #'.$id.' não existe!', 'request' => $id];
        } else {
            $em->remove($user);
            $em->flush();
            $response = ['message' => 'success', 'data' => $user->getId(), 'request' => $id];
        }

        return new JsonResponse($response);
    }

    public function passwordUser(Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {
        $role = $request->request->get('role');

        $em = $this->getDoctrine()->getManager();

        if ($request->query->get('id')) {
            $id = $request->query->get('id');
            //$user = $em->getRepository(Admin::class)->find($id);
            'ROLE_ADMIN' == $role ? $user = $em->getRepository(Admin::class)->find($id) : $user = $em->getRepository(Manager::class)->find($id);

            $passwordForm = $this->createForm(ChangePasswordType::class, $user);

            return $this->render('admin/user-password.html', [
                'passwordForm' => $passwordForm->createView(),
                //'editFormErrors' => true
            ]);
        } else {
            $id = $request->request->get('id');

            //$user = $em->getRepository(Admin::class)->find($id);
            if ('ROLE_ADMIN' == $role) {
                $user = $em->getRepository(Admin::class)->find($id);
            } elseif ('ROLE_MANAGER' == $role) {
                $user = $em->getRepository(Manager::class)->find($id);
            } else {
                $user = $em->getRepository(SuperUser::class)->find($id);
            }

            //$role == "ROLE_ADMIN"?$user = $em->getRepository(Admin::class)->find($id): $user = $em->getRepository(Manager::class)->find($id);

            $form = $this->createForm(ChangePasswordType::class, $user);

            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                // dd($user, $user->getPlainPassword());
                // exit;
                $password = $passwordEncoder->encodePassword($user, $user->getPlainPassword());

                $user->setPassword($password);
                $em->persist($user);
                $em->flush();

                $response = ['message' => 'success', 'data' => $user->getId(), 'request' => $id];
            } else {
                $errorMessages = [];

                foreach ($form['plainPassword']->getErrors(true) as $error) {
                    $errorMessages[] = $error->getMessage();
                }

                $response = ['message' => 'fail', 'data' => $errorMessages, 'request' => $id];
            }

            return new JsonResponse($response);
        }
    }

    public function passwordRecovery(Request $request, UserPasswordEncoderInterface $passwordEncoder, TranslatorInterface $translator)
    {
        $email = $request->request->get('email');

        $em = $this->getDoctrine()->getManager();
        $locale = $request->getlocale();

        $user = $em->getRepository(User::class)->findOneBy(['email' => $email]);

        if (!$user) {
            $response = [
                'status' => 0,
                'message' => 'invalid email',
                'locale' => $locale,
            ];

            return new JsonResponse($response);
        }

        $plainPassword = substr(md5(rand()), 0, 8);
        $password = $passwordEncoder->encodePassword($user, $plainPassword);

        $user->setPassword($password);
        $em->persist($user);
        $em->flush();

        $company = $em->getRepository(Company::class)->find(1);
        $t = $this->translations($translator, $locale);

        $result = $this->sendEmail($company, $plainPassword, $user, $t, $request);

        if ($result) {
            $response = [
                'status' => 1,
                'message' => 'Sucess',
            ];

            return new JsonResponse($response);
        } else {
            $response = [
                'status' => 0,
                'message' => 'Failed to send email',
            ];

            return new JsonResponse($response);
        }
    }

    private function sendEmail($company, $password, $user, $translator, $request)
    {
        $template = 'password_recovery';

        $transport = (new \Swift_SmtpTransport($company->getEmailSmtp(), $company->getEmailPort(), $company->getEmailCertificade()))
            ->setUsername($company->getEmail())
            ->setPassword($company->getEmailPass());

        $mailer = new \Swift_Mailer($transport);

        $message = (new \Swift_Message($translator['recovery']))
            ->setFrom([$company->getEmail() => $company->getName()])
            ->setBcc($user->getEmail())
            ->setTo([$user->getEmail() => $user->getUsername()]) //$company->getEmail2() nauticdrive.fotos@gmail.com roman.bajireanu@intouchbiz.com
            ->addPart($translator['recovery'], 'text/plain')
            ->setBody(
                $this->renderView(
                   'emails/'.$template.'.twig',
                   ['company' => $company,
                   'logo' => $request->getHost().'/upload/gallery/'.$company->getLogo(),
                   'password' => $password,
                   'translator' => $translator,
                   ]
                ),
            'text/html'
        );

        return $mailer->send($message);
    }

    private function translations($translator, $locale)
    {
        return ['hello' => $translator->trans('hello', [], 'messages', $locale),
                     'team' => $translator->trans('team', [], 'messages', $locale),
                     'recovery' => $translator->trans('password_recovery.recovery', [], 'messages', $locale),
                     'password_new' => $translator->trans('password_recovery.password_new', [], 'messages', $locale),
            ];
    }

    public function dataUser($userType)
    {
        $u = [];

        if ($userType instanceof Manager) {
            $u[] = [
                'id' => $userType->getId(),
                'email' => $userType->getEmail(),
                'username' => $userType->getUsername(),
                'status' => $userType->getStatus(),
                'role' => $userType->getRoles(),
            ];
        } else {
            foreach ($userType as $user) {
                $u[] = [
                    'id' => $user->getId(),
                    'email' => $user->getEmail(),
                    'username' => $user->getUsername(),
                    'status' => $user->getStatus(),
                    'role' => $user->getRoles(),
                ];
            }
        }

        return $u;
    }

    public function userEdit(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        // dd($request->request);
        // exit;

        $user = $em->getRepository(User::class)->find($request->request->get('id'));

        if (!$user) {
            $response = [
                'status' => 0,
                'message' => 'menu not found',
                'userid' => $id, ];

            return new JsonResponse($response);
        }
        $form = $this->createForm(AdminType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                try {
                    // dd($user->getPassword());
                    // exit;
                    // $user->setPlainPassword($user->getPassword());
                    // $user->setPassword($user->getPassword());
                    $em->persist($user);
                    $em->flush();

                    $response = [
                        'status' => 1,
                        'message' => 'success',
                        'data' => $user->getId(),
                    ];

                    return new JsonResponse($response);
                } catch (DBALException $e) {
                    $a = ['Contate administrador sistema sobre: '.$e->getMessage()];
                    $response = [
                        'status' => 0,
                        'message' => 'fail',
                        'data' => $a, ];

                    return new JsonResponse($response);
                }
            } else {
                $response = [
                    'status' => 0,
                    'message' => 'fail',
                    'data' => $this->getErrorMessages($form),
                ];

                return new JsonResponse($response);
            }
        }

        return new JsonResponse($response);
    }

    public function userShowEdit(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $locale = $request->getlocale();
        $id = $request->request->get('id');

        $user = $em->getRepository(User::class)->findOneBy(['id' => $id]);

        if ($user instanceof SuperUser) {
            $user = $em->getRepository(SuperUser::class)->find($id);
        } elseif ($user instanceof Admin) {
            $user = $em->getRepository(Admin::class)->find($id);
        } else {
            $user = $em->getRepository(Manager::class)->find($id);
        }

        $form = $this->createForm(AdminType::class, $user);

        // $form->get('plainPassword')->setData($user->getPassword());

        return $this->render('admin/user-edit.html', [
            'form' => $form->createView(),
            'locale' => $locale,
            'userId' => $id,
        ]);
    }

    /**
     * Returns Doctrine entity name.
     *
     * @param mixed $entity
     *
     * @return string
     *
     * @throws \Exception
     */
    private function getEntityName($entity)
    {
        $em = $this->getDoctrine()->getManager();
        try {
            $entityName = $em->getMetadataFactory()->getMetadataFor(get_class($entity))->getName();
        } catch (MappingException $e) {
            throw new \Exception('Given object '.get_class($entity).' is not a Doctrine Entity. ');
        }

        return $entityName;
    }

    protected function getErrorMessages(\Symfony\Component\Form\Form $form)
    {
        $errors = [];
        $err = [];
        foreach ($form->getErrors() as $key => $error) {
            $errors[] = $error->getMessage();
        }

        foreach ($form->all() as $child) {
            if (!$child->isValid()) {
                $errors[] = $this->getErrorMessages($child);
            }
        }

        foreach ($errors as $error) {
            $err[] = $error;
        }

        return $err;
    }
}
