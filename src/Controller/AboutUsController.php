<?php

namespace App\Controller;

use App\Entity\AboutUs;
use App\Entity\Locales;
use App\Form\AboutUsType;
use Doctrine\DBAL\DBALException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class AboutUsController extends AbstractController
{
    public function aboutUs(Request $request, ValidatorInterface $validator)
    {
        $em = $this->getDoctrine()->getManager();

        if ($request->request->get('id')) {
            $aboutUs = $em->getRepository(AboutUs::class)->find($request->request->get('id'));
            $form = $this->createForm(AboutUsType::class, $aboutUs);
        } else {
            $aboutUs = $em->getRepository(AboutUs::class)->findAll();
            $form = $this->createForm(AboutUsType::class);
        }

        $locales = $em->getRepository(Locales::class)->findAll();

        if ($request->isXmlHttpRequest() && $request->request->get($form->getName())) {
            $form->submit($request->request->get($form->getName()));

            if ($form->isSubmitted()) {
                $aboutUs->setLocales($locales);

                if ($form->isValid()) {
                    $em = $this->getDoctrine()->getManager();
                    $aboutUs = $form->getData();

                    $aboutUs->setLocales($locales);

                    $em->persist($aboutUs);
                    $em->flush();

                    $response = [
                        'result' => 1,
                        'message' => 'success',
                        'data' => $aboutUs->getId(), ];
                } else {
                    $response = [
                        'result' => 0,
                        'message' => 'fail',
                        'is_ok' => $form['locales']->getData(),
                        'data' => $this->getErrorMessages($form),
                    ];
                }
            } else {
                $response = [
                    'result' => 2,
                    'message' => 'fail not submitted',
                    'data' => '', ];
            }

            return new JsonResponse($response);
        }

        return $this->render('admin/about-us.html', [
            'form' => $form->createView(),
            'aboutUs' => $aboutUs,
            'locales' => $locales,
        ]);

        return $this->render('admin/about-us.html');
    }

    public function aboutUsEdit(Request $request, ValidatorInterface $validator)
    {
        $em = $this->getDoctrine()->getManager();

        $aboutUs = $em->getRepository(AboutUs::class)->find($request->request->get('id'));

        $form = $this->createForm(AboutUsType::class, $aboutUs);

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $aboutUs = $form->getData();

                try {
                    $em->persist($aboutUs);
                    $em->flush();

                    $response = [
                        'status' => 1,
                        'message' => 'Sucesso',
                        'data' => 'O registo '.$aboutUs->getId().' foi gravado.', ];
                } catch (DBALException $e) {
                    $a = ['Contate administrador sistema sobre: '.$e->getMessage()];

                    $response = [
                        'status' => 0,
                        'message' => 'fail',
                        'data' => $a, ];
                }
            } else {
                $response = [
                    'result' => 0,
                    'message' => 'fail',
                    'data' => $this->getErrorMessages($form),
                ];
            }
        }

        return new JsonResponse($response);
    }

    public function aboutUsDelete(Request $request)
    {
        $response = [];
        $aboutUsId = $request->request->get('id');
        $em = $this->getDoctrine()->getManager();

        $aboutUs = $em->getRepository(AboutUs::class)->find($aboutUsId);

        if (!$aboutUs) {
            $response = ['message' => 'fail', 'status' => 'Registo #'.$aboutUsId.' não existe.'];
        } else {
            $em->remove($aboutUs);
            $em->flush();

            $response = ['message' => 'success', 'status' => $aboutUsId];
        }

        return new JsonResponse($response);
    }

    public function menuShow(Request $request)
    {
        $response = [];

        $em = $this->getDoctrine()->getManager();

        $locales = $em->getRepository(Locales::class)->findOneBy(['name' => $this->session->get('_locale')->getName()]);

        $aboutUs = $em->getRepository(AboutUs::class)->findOneBy(['locales' => $locales]);

        $response = !$aboutUs ?
            ['status' => 0, 'message' => 'Registo não encontrado', 'data' => null]
            :
            ['status' => 1, 'message' => $aboutUs->getName(), 'data' => $aboutUs->getRgpdHtml()];

        return new JsonResponse($response);
    }

    protected function getErrorMessages(\Symfony\Component\Form\Form $form)
    {
        $errors = [];
        foreach ($form->getErrors() as $key => $error) {
            $errors[] = $error->getMessage();
        }

        foreach ($form->all() as $child) {
            if (!$child->isValid()) {
                $errors[] = $this->getErrorMessages($child);
            }
        }

        return $errors;
    }
}
