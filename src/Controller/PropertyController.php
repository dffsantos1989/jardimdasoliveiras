<?php

namespace App\Controller;

use App\Entity\Admin;
use App\Entity\Company;
use App\Entity\Locales;
use App\Entity\Property;
use App\Entity\PropertyBulletin;
use App\Entity\SuperUser;
use App\Form\PropertyType;
use App\Service\Host;
use App\Service\MoneyFormatter;
use App\Service\MoneyParser;
use Doctrine\DBAL\DBALException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\Translation\TranslatorInterface;

class PropertyController extends AbstractController
{
    public function propertyNew(Request $request, TranslatorInterface $translator)
    {
        $locale = $request->getlocale();
        $em = $this->getDoctrine()->getManager();
        //$company = $em->getRepository(Company::class)->find(1);
        //$locales = $em->getRepository(Locales::class)->findAll();
        $user = $this->getUser();
        $housing_bulletin_translate = $translator->trans('property.housing_bulletin', [], 'messages', $locale);

        $property = new Property();
        $form = $this->createForm(PropertyType::class, $property, ['user' => $user, 'housing_bulletin' => $housing_bulletin_translate]); //, array("user"=>$user)
        $form->handleRequest($request);

        return $this->render('admin/property-new.html', [
            'form' => $form->createView(),
            //'company' => $company,
            //'locales' => $locales,
            'locale' => $locale,
            // 'menus' => $m,
        ]);
    }

    public function propertyList(Request $request, MoneyFormatter $moneyFormatter)
    {
        $em = $this->getDoctrine()->getManager();
        $loggedUser = $this->getUser();

        if ($loggedUser instanceof SuperUser || $loggedUser instanceof Admin) {
            $properties = $em->getRepository(Property::class)->findBy(['deleted' => false]); //,['orderBy' => 'ASC']
            //$properties = $em->getRepository(Property::class)->findAll();
        } else {
            $properties = $em->getRepository(Property::class)->findBy(['user' => $loggedUser->getId(), 'deleted' => false]);
        }

        $property = $this->dataProperty($properties, $moneyFormatter);

        return $this->render('admin/property-list.html', [
            'properties' => $property,
        ]);
    }

    public function propertyDelete(Request $request)
    {
        $response = [];
        $id = $request->request->get('id');

        $em = $this->getDoctrine()->getManager();

        $property = $em->getRepository(Property::class)->find($id);
        // $bulletin = $em->getRepository(PropertyBulletin::class)->findOneBy(["property"=>$id]);

        // if ($bulletin) {
        //     $em->remove($bulletin);
        //     $em->flush();
        // }

        if (!$property) {
            return new JsonResponse(['status' => 0, 'message' => 'Property #'.$id.' nÃ£o existe.']);
        }

        $property->setDeleted(true);
        $em->persist($property);
        $em->flush();

        return new JsonResponse(['status' => 1, 'message' => 'Property deleted']);
    }

    public function propertyShowEdit(Request $request, MoneyFormatter $moneyFormatter)
    {
        $em = $this->getDoctrine()->getManager();

        $locale = $request->getlocale();
        $id = $request->request->get('id');

        $property = $em->getRepository(Property::class)->find($id);

        //dd($property->getDeposit()->getAmount());
        //exit;

        $form = $this->createForm(PropertyType::class, $property);

        $deposit = $moneyFormatter->format($property->getDeposit()); //->getAmount()

        $form->get('deposit')->setData($deposit);

        // if($property) {

        // }

        return $this->render('admin/property-edit.html', [
            'form' => $form->createView(),
            'locale' => $locale,
            'propertyId' => $id,
        ]);
    }

    public function propertyAdd(Request $request, Host $host, MoneyParser $moneyParser)
    {
        //dd($request->request);
        //exit;

        $host = $host->getHost($request);

        $property = new Property();

        $em = $this->getDoctrine()->getManager();

        $locales = $em->getRepository(Locales::class)->findAll();

        $form = $this->createForm(PropertyType::class, $property);

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $property = $form->getData();
                $deposit = $moneyParser->parse($request->request->get('PropertyType')['deposit']);
                try {
                    //dd($property->getDetails());
                    $property->setToken($this->generateToken());
                    if ($property->getDetails()) {
                        $property->addDetails($property->getDetails()[0]);
                    }
                    $property->setDeposit($deposit);

                    $em->persist($property);
                    $em->flush();

                    foreach ($locales as $locale) {
                        $urls[] = $host.'/'.$locale->getName().'/property-bulletin/'.$property->getToken();
                        $paths[] = $locale->getFileName();
                    }

                    $response = [
                            'status' => 1,
                            'message' => 'success',
                            'data' => $urls,
                            'paths' => $paths,
                        ];
                } catch (DBALException $e) {
                    $a = ['Contate administrador sistema sobre: '.$e->getMessage()];
                    $response = [
                                'status' => 0,
                                'message' => 'fail',
                                'data' => $a, ];
                }
            } else {
                $response = [
                        'status' => 0,
                        'message' => 'fail',
                        'data' => $this->getErrorMessages($form),
                    ];
            }
        } else {
            $response = [
                    'status' => 2,
                    'message' => 'fail not submitted',
                    'data' => '', ];
        }

        return new JsonResponse($response);
    }

    public function propertyEdit(Request $request, MoneyParser $moneyParser)
    {
        $em = $this->getDoctrine()->getManager();

        //dd($request->request);
        //exit;

        $property = $em->getRepository(Property::class)->find($request->request->get('id')); //$request->request->get('PropertyType')["id"]

        if (!$property) {
            $response = [
                'status' => 0,
                'message' => 'menu not found',
                'propertyid' => $id, ];

            return new JsonResponse($response);
        }
        $form = $this->createForm(PropertyType::class, $property);

        $form->handleRequest($request);

        $deposit = $moneyParser->parse($request->request->get('PropertyType')['deposit']);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                try {
                    //$property->addDetails($property->getDetails()[0]);
                    $property->setDeposit($deposit);

                    $em->persist($property);
                    $em->flush();

                    $response = [
                        'status' => 1,
                        'message' => 'success',
                        'data' => $property->getId(),
                    ];

                    return new JsonResponse($response);
                } catch (DBALException $e) {
                    $a = ['Contate administrador sistema sobre: '.$e->getMessage()];
                    $response = [
                        'status' => 0,
                        'message' => 'fail',
                        'data' => $a, ];

                    return new JsonResponse($response);
                }
            } else {
                $response = [
                    'status' => 0,
                    'message' => 'fail',
                    'data' => $this->getErrorMessages($form),
                ];

                return new JsonResponse($response);
            }
        }

        return new JsonResponse($response);
    }

    public function validateNif(Request $request)
    {
        $nif = $request->query->get('PropertyType')['hotelUnitCode'];

        if (!is_numeric($nif) || 9 != strlen($nif)) {
            return false;
        } else {
            $nifSplit = str_split($nif);
            if (
                in_array($nifSplit[0], [1, 2, 3, 5, 6, 8])
            /*
                ||
                in_array($nifSplit[0].$nifSplit[1], array(45, 70, 71, 72, 74, 75, 77, 78, 79, 90, 91, 98, 99))
                ||
                $ignore
            */
            ) {
                $checkDigit = 0;
                for ($i = 0; $i < 8; ++$i) {
                    $checkDigit += $nifSplit[$i] * (10 - $i - 1);
                }
                $checkDigit = 11 - ($checkDigit % 11);
                if ($checkDigit >= 10) {
                    $checkDigit = 0;
                }
                if ($checkDigit == $nifSplit[8]) {
                    return new Response(1);
                } else {
                    return new Response(0);
                }
            } else {
                return new Response(0);
            }
        }
    }

    public function validateZipCode(Request $request)
    {
        $cod_postal2 = '';
        $cod_postal = $request->query->get('PropertyType')['details'][0]['postalCode'];

        $morada['city'] = '';
        $morada['street_name'] = '';
        if ('' != $cod_postal) {
            if ('' != $cod_postal2) {
                $zipcode = $cod_postal.'-'.$cod_postal2;
            } else {
                $zipcode = $cod_postal;
            }
            $api_key = '&key=AIzaSyCPSSY5P1eoGdzdp6KJ_acb1VQr_ubtorw';
            $url = 'https://maps.googleapis.com/maps/api/geocode/xml?sensor=false&address=portugal:'.$zipcode.$api_key;
            $geocode = simplexml_load_file($url);
            if (isset($geocode->result->address_component[1]->long_name)) {
                $morada['city'] = $geocode->result->address_component[1]->long_name;
                if (isset($geocode->result->geometry->location->lat) && isset($geocode->result->geometry->location->lng)) {
                    $lat = $geocode->result->geometry->location->lat;
                    $lng = $geocode->result->geometry->location->lng;
                    $url2 = 'https://maps.googleapis.com/maps/api/geocode/xml?sensor=false&latlng='.$lat.','.$lng.$api_key.'';
                    $geocode2 = simplexml_load_file($url2);
                    if (isset($geocode2->result->address_component[1]->long_name)) {
                        $morada['street_name'] = $geocode2->result->address_component[1]->long_name;
                    }
                }
            }
        }

        if ((bool) $morada['city']) {
            return new Response(1);
        } else {
            return new Response(0);
        }
    }

    public function dataProperty($properties, $moneyFormatter)
    {
        //$property = [];

        foreach ($properties as $p) {
            $translation = [];
            $details = [];

            foreach ($p->getTranslation() as $translated) {
                $translation[] = [
                        'local' => $translated->getLocales()->getName(),
                        'reasons' => $translated->getReasons(),
                        'instructions' => $translated->getInstructions(),
                        'local_id' => $translated->getLocales()->getId(),
                    ];
            }

            foreach ($p->getDetails() as $detail) {
                $details = [
                    'abbreviation' => $detail->getAbbreviation(),
                    'address' => $detail->getAddress(),
                    'locality' => $detail->getLocality(),
                    'postal_code' => $detail->getPostalCode(),
                    'postal_zone' => $detail->getPostalZone(),
                    'telephone' => $detail->getTelephone(),
                    'contact_name' => $detail->getContactName(),
                    'contact_email' => $detail->getContactEmail(),
                ];
            }

            $property[] = [
                'id' => $p->getId(),
                'guest_bulletin' => $p->getGuestBulletin(),
                'security_deposit' => $p->getSecurityDeposit(),
                'deposit' => $moneyFormatter->format($p->getDeposit()).'€',
                'sef_api_key' => $p->getSefApiKey(),
                'hotel_unit_code' => $p->getHotelUnitCode(),
                'establishment' => $p->getEstablishment(),
                'name' => $p->getName(),
                'token' => $p->getToken(),
                'translation' => $translation,
                'details' => $details,
            ];
        }

        return $property;
    }

    private function defaultUserLocale(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $defaultUserLocale = 'en' == $request->getLocale() || 'en' == $request->getLocale() ? 'en' : 'pt';
        $userLocale = $em->getRepository(Locales::class)->findOneBy(['name' => $defaultUserLocale]);

        return $userLocale;
    }

    private function generateToken()
    {
        $em = $this->getDoctrine()->getManager();
        $token = strtoupper(substr(md5(rand()), 0, 12));
        if ($em->getRepository(Property::class)->findOneBy(['token' => $token])) {
            return $this->generateToken();
        } else {
            return $token;
        }
    }

    protected function getErrorMessages(\Symfony\Component\Form\Form $form)
    {
        $errors = [];
        $err = [];
        foreach ($form->getErrors() as $key => $error) {
            $errors[] = $error->getMessage();
        }

        foreach ($form->all() as $child) {
            if (!$child->isValid()) {
                $errors[] = $this->getErrorMessages($child);
            }
        }

        foreach ($errors as $error) {
            $err[] = $error;
        }

        return $err;
    }
}
