<?php

namespace App\Controller;

use App\Entity\Locales;
use App\Entity\Seo;
use App\Form\SeoType;
use Doctrine\DBAL\DBALException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class SeoController extends AbstractController
{
    public function seo(Request $request, ValidatorInterface $validator)
    {
        $em = $this->getDoctrine()->getManager();

        if ($request->request->get('id')) {
            $seo = $em->getRepository(Seo::class)->find($request->request->get('id'));
            $form = $this->createForm(Seo::class, $seo);
        } else {
            $seo = $em->getRepository(Seo::class)->findAll();
            $form = $this->createForm(SeoType::class);
        }

        $locales = $em->getRepository(Locales::class)->findAll();

        if ($request->isXmlHttpRequest() && $request->request->get($form->getName())) {
            $form->submit($request->request->get($form->getName()));

            if ($form->isSubmitted()) {
                $seo->setLocales($locales);

                if ($form->isValid()) {
                    $em = $this->getDoctrine()->getManager();
                    $seo = $form->getData();

                    $seo->setLocales($locales);

                    $em->persist($seo);
                    $em->flush();

                    $response = [
                        'result' => 1,
                        'message' => 'success',
                        'data' => $seo->getId(), ];
                } else {
                    $response = [
                        'result' => 0,
                        'message' => 'fail',
                        'is_ok' => $form['locales']->getData(),
                        'data' => $this->getErrorMessages($form),
                    ];
                }
            } else {
                $response = [
                    'result' => 2,
                    'message' => 'fail not submitted',
                    'data' => '', ];
            }

            return new JsonResponse($response);
        }
        // dd($locales);

        return $this->render('admin/seo.html', [
            'form' => $form->createView(),
            'seos' => $seo,
            'locales' => $locales,
        ]);

        //return $this->render('admin/seo.html');
    }

    public function seoEdit(Request $request, ValidatorInterface $validator)
    {
        $em = $this->getDoctrine()->getManager();

        $seo = $em->getRepository(Seo::class)->find($request->request->get('id'));

        $form = $this->createForm(SeoType::class, $seo);

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $seo = $form->getData();

                try {
                    $em->persist($seo);
                    $em->flush();

                    $response = [
                        'status' => 1,
                        'message' => 'Sucesso',
                        'data' => 'O registo '.$seo->getId().' foi gravado.', ];
                } catch (DBALException $e) {
                    $a = ['Contate administrador sistema sobre: '.$e->getMessage()];

                    $response = [
                        'status' => 0,
                        'message' => 'fail',
                        'data' => $a, ];
                }
            } else {
                $response = [
                    'result' => 0,
                    'message' => 'fail',
                    'data' => $this->getErrorMessages($form),
                ];
            }
        }

        return new JsonResponse($response);
    }

    public function seoDelete(Request $request)
    {
        $response = [];
        $seoId = $request->request->get('id');
        $em = $this->getDoctrine()->getManager();

        $seo = $em->getRepository(Seo::class)->find($seo);

        if (!$seo) {
            $response = ['message' => 'fail', 'status' => 'Registo #'.$seo.' não existe.'];
        } else {
            $em->remove($seo);
            $em->flush();

            $response = ['message' => 'success', 'status' => $seoId];
        }

        return new JsonResponse($response);
    }

    public function menuShow(Request $request)
    {
        $response = [];

        $em = $this->getDoctrine()->getManager();

        $locales = $em->getRepository(Locales::class)->findOneBy(['name' => $this->session->get('_locale')->getName()]);

        $seo = $em->getRepository(SeoData::class)->findOneBy(['locales' => $locales]);

        $response = !$seo ?
            ['status' => 0, 'message' => 'Registo não encontrado', 'data' => null]
            :
            ['status' => 1, 'message' => $seo->getName(), 'data' => $seo->getRgpdHtml()];

        return new JsonResponse($response);
    }

    protected function getErrorMessages(\Symfony\Component\Form\Form $form)
    {
        $errors = [];
        foreach ($form->getErrors() as $key => $error) {
            $errors[] = $error->getMessage();
        }

        foreach ($form->all() as $child) {
            if (!$child->isValid()) {
                $errors[] = $this->getErrorMessages($child);
            }
        }

        return $errors;
    }
}
