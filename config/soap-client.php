<?php

use Phpro\SoapClient\CodeGenerator\Assembler;
use Phpro\SoapClient\CodeGenerator\Config\Config;
use Phpro\SoapClient\CodeGenerator\Rules;
use Phpro\SoapClient\Soap\Driver\ExtSoap\ExtSoapEngineFactory;
use Phpro\SoapClient\Soap\Driver\ExtSoap\ExtSoapOptions;

return Config::create()
    ->setEngine(ExtSoapEngineFactory::fromOptions(
        ExtSoapOptions::defaults('https://siba.sef.pt/bawsdev/boletinsalojamento.asmx?wsdl', [])
            ->disableWsdlCache()
    ))
    ->setTypeDestination('src\Type')
    ->setTypeNamespace('App\Type')
    ->setClientDestination('src')
    ->setClientName('sefClient')
    ->setClientNamespace('App')
    ->setClassMapDestination('src')
    ->setClassMapName('sefClassmap')
    ->setClassMapNamespace('App')
    ->addRule(new Rules\AssembleRule(new Assembler\GetterAssembler(new Assembler\GetterAssemblerOptions())))
    ->addRule(new Rules\AssembleRule(new Assembler\ImmutableSetterAssembler()))
;
